//
//  PagedVC.h
//  pageController
//
//  Created by Jose Catala on 13/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PagedVCDelegate <NSObject>

@optional

- (void) itemTapped:(NSDictionary *)item;

@end

@interface PagedVC : UIViewController

- (id) initWithFrame:(CGSize)size andImages:(NSArray *)images;

@property (weak, nonatomic) id<PagedVCDelegate>delegate;

@end
