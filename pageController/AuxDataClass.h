//
//  AuxDataClass.h
//  pageController
//
//  Created by Jose Catala on 13/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuxDataClass : NSObject

+ (NSArray *) arrayOfImages;

@end
