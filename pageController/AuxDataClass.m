//
//  AuxDataClass.m
//  pageController
//
//  Created by Jose Catala on 13/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "AuxDataClass.h"

@implementation AuxDataClass

+ (NSArray *) arrayOfImages
{
    return @[@"http://www.rapidresponsecomputer.com/wp-content/uploads/2016/02/smartphone-silhouette-image.png",
             @"http://i2.expansys.com/img/p/265928/apple-iphone-6-a1586-47.jpg",
             @"https://d3nj7353mvgauu.cloudfront.net/media/categories/iphone-77-plus-40-b1ac.png"];
}
@end
