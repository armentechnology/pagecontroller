//
//  PagedVC.m
//  pageController
//
//  Created by Jose Catala on 13/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "PagedVC.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface PagedVC ()
{
    NSArray *imagesArray;
    
    NSInteger index;
    
    
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintImgCenter;
@property (weak, nonatomic) IBOutlet UIImageView *imgItem;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControlItems;

@end

@implementation PagedVC

- (id) initWithFrame:(CGSize)size andImages:(NSArray *)images
{
    self = [super initWithNibName:@"PagedVC" bundle:nil];
    
    if (self)
    {
        self.view.frame = CGRectMake(0, 0, size.width, size.height);
        
        imagesArray = [[NSArray alloc] initWithArray:images ? images : @[]];
        
        index = 0;
        
        //Download the images
        if (imagesArray.count > 0)
        {
            id obj = imagesArray[0];
            
            if ([obj isKindOfClass:[NSString class]])
            {
                [self imageWithUrlString:(NSString *)obj];
            }
        }
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_imgItem setMultipleTouchEnabled:YES];
    
    [_imgItem setUserInteractionEnabled:YES];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [_imgItem addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [_imgItem addGestureRecognizer:swiperight];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgTapped:)];
    [_imgItem addGestureRecognizer:tap];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark GESTURES

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if (index == imagesArray.count - 1) return;
    
    index ++;
    
    _pageControlItems.currentPage = index;
    
    NSString *newImageURLString = imagesArray[index];
    
    float outLeft = - _imgItem.frame.size.width / 2;
    
    float outRight = self.view.frame.size.width + _imgItem.frame.size.width / 2;
    
    _constraintImgCenter.constant = outLeft;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        [self.view layoutIfNeeded];
        
    }completion:^(BOOL finished) {
        
        [self imageWithUrlString:newImageURLString];
        
        _constraintImgCenter.constant = outRight;
        
        [self.view layoutIfNeeded];
        
        _constraintImgCenter.constant = 0;
        
        [UIView animateWithDuration:0.2 animations:^{
            
            [self.view layoutIfNeeded];
        }];
    }];
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if (index == 0) return;
    
    index --;
    
    _pageControlItems.currentPage = index;
    
    NSString *newImageURLString = imagesArray[index];
    
    float outLeft = - _imgItem.frame.size.width / 2;
    
    float outRight = self.view.frame.size.width + _imgItem.frame.size.width / 2;
    
    _constraintImgCenter.constant = outRight;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        [self.view layoutIfNeeded];
        
    }completion:^(BOOL finished) {
        
        [self imageWithUrlString:newImageURLString];
        
        _constraintImgCenter.constant = outLeft;
        
        [self.view layoutIfNeeded];
        
        _constraintImgCenter.constant = 0;
        
        [UIView animateWithDuration:0.2 animations:^{
            
            [self.view layoutIfNeeded];
        }];
    }];
}

- (void)imgTapped:(UITapGestureRecognizer *)sender
{
    NSDictionary *dic = @{@"index":@(index)};
    
    [_delegate itemTapped:dic];
}

- (void) imageWithUrlString:(NSString *)string
{
    [_imgItem setImageWithURL:[NSURL URLWithString:string] placeholderImage:[UIImage imageNamed:@"image-placeholder"]];
}

@end
