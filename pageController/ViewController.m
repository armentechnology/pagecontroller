//
//  ViewController.m
//  pageController
//
//  Created by Jose Catala on 13/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "ViewController.h"
#import "PagedVC.h"
#import "AuxDataClass.h"

@interface ViewController ()<PagedVCDelegate>
{
    PagedVC *pagedItems;
}


@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
   
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    pagedItems = [[PagedVC alloc]initWithFrame:_viewContainer.frame.size andImages:[AuxDataClass arrayOfImages]];
    pagedItems.delegate = self;
    
    [_viewContainer insertSubview:pagedItems.view atIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark PAGEDVC DELEGATE

- (void) itemTapped:(NSDictionary *)item
{
    
}

@end
